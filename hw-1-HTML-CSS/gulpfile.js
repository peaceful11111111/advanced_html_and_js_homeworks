const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const babel = require('gulp-babel');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();
const clean = require('gulp-clean');

const path = {
    dist: {
        html: 'dist',
        css: 'dist/css',
        js: 'dist/js',
        img: 'dist/img',
        self: 'dist',
    },
    src: {
        html: 'src/*.html',
        scss: 'src/scss/*.scss',
        img: 'src/img/*.*',
        js: 'src/js/*.js'
    }
};

// FUNCTIONS  -----

const htmlBuilder = () => (
  gulp.src(path.src.html)
      .pipe(gulp.dest(path.dist.html))
      .pipe(browserSync.stream())
);

const scssBuilder = () => (
    gulp.src(path.src.scss)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer(['> 0.01%', 'last 100 versions']))
        .pipe(gulp.dest(path.dist.css))
        .pipe(browserSync.stream())
);

const jsBuilder = () => (
  gulp.src(path.src.js)
      .pipe(concat('script.js'))
      .pipe(babel ({
          presets: ['@babel/env']
      }))
      .pipe(gulp.dest(path.dist.js))
      .pipe(browserSync.stream())
);

const imgBuilder = () => (
    gulp.src(path.src.img)
        .pipe(gulp.dest(path.dist.img))
);

const cleanDist = () => (
  gulp.src(path.dist.self, {allowEmpty: true})
      .pipe(clean())
);

// WATCHER  -----

const watcher = () => {
  browserSync.init({
      server: {
          baseDir: './dist'
      }
  });

  gulp.watch(path.src.html, htmlBuilder).on('change', browserSync.reload);
  gulp.watch(path.src.scss, scssBuilder).on('change', browserSync.reload);
  gulp.watch(path.src.js, jsBuilder).on('change', browserSync.reload);
  gulp.watch(path.src.img, imgBuilder).on('change', browserSync.reload);
};

// TASKS   -----

gulp.task('html', htmlBuilder);
gulp.task('scss', scssBuilder);
gulp.task('js', jsBuilder);
gulp.task('img', imgBuilder);

gulp.task('default', gulp.series(
    cleanDist,
    gulp.parallel(htmlBuilder, scssBuilder, jsBuilder, imgBuilder),
    watcher
));








