let xhr = new XMLHttpRequest();
xhr.open('GET', 'https://swapi.co/api/films/');
xhr.responseType = 'json';
xhr.send();
xhr.onload = function () {
  if (xhr.status !== 200) {
    console.log(`Error ${xhr.status} Text ${xhr.statusText}`);
  } else {
    let response = xhr.response;
    console.log(response.results);
    let resultsArr = response.results;

    let title = resultsArr.map(item => `
<div class="episode-container">
<h1 class="episode-title">${item.title}</h1>
<p class="">Episode: ${item.episode_id}</p>
<p class="crawl">${item.opening_crawl}</p>
<div class="episode-people">
<button class="list">Список персонажей</button>
</div>
</div>
`).join('');

    const div = document.getElementById('episodes');
    div.innerHTML = title;
    div.addEventListener('click', function (event) {
      const titleClass = document.getElementsByClassName('list');
      const container = document.getElementsByClassName('episode-people');


      if (event.target === titleClass[0]) {
        titleClass[0].style.display = 'none';

        let people = response.results[0].characters;

        people.forEach((item) => {

          let xhr = new XMLHttpRequest();
          xhr.open('GET', item);
          xhr.responseType = 'json';
          xhr.send();
          xhr.onload = function () {
            if (xhr.status !== 200) {
              console.log(`${xhr.status}: ${xhr.statusText}`);
            } else {
              let response = xhr.response;
              let span = document.createElement('span');
              span.classList.add('star-wars-players');
              span.innerText = `${response.name}, `;
              container[0].append(span);
            }
          };
          xhr.onerror = function () {
            alert('People undefined')
          }
        })
      } else if (event.target === titleClass[1]) {
        titleClass[1].style.display = 'none';

        let people = response.results[1].characters;

        people.forEach((item) => {
          let xhr = new XMLHttpRequest();
          xhr.open('GET', item);
          xhr.responseType = 'json';
          xhr.send();
          xhr.onload = function () {
            if (xhr.status !== 200) {
              console.log(`${xhr.status}: ${xhr.statusText}`);
            } else {
              let response = xhr.response;
              let span = document.createElement('span');
              span.classList.add('star-wars-players');
              span.style.color = 'brown';
              span.innerText = `${response.name}, `;
              container[1].append(span);
            }
          };
          xhr.onerror = function () {
            alert('People undefined')
          }
        })
      } else if (event.target === titleClass[2]) {
        titleClass[2].style.display = 'none';

        let people = response.results[2].characters;

        people.forEach((item) => {
          let xhr = new XMLHttpRequest();
          xhr.open('GET', item);
          xhr.responseType = 'json';
          xhr.send();
          xhr.onload = function () {
            if (xhr.status !== 200) {
              console.log(`${xhr.status}: ${xhr.statusText}`);
            } else {
              let response = xhr.response;
              let span = document.createElement('span');
              span.classList.add('star-wars-players');
              span.style.color = 'pink';
              span.innerText = `${response.name}, `;
              container[2].append(span);
            }
          };
          xhr.onerror = function () {
            alert('People undefined')
          }
        })
      } else if (event.target === titleClass[3]) {
        titleClass[3].style.display = 'none';

        let people = response.results[3].characters;

        people.forEach((item) => {
          let xhr = new XMLHttpRequest();
          xhr.open('GET', item);
          xhr.responseType = 'json';
          xhr.send();
          xhr.onload = function () {
            if (xhr.status !== 200) {
              console.log(`${xhr.status}: ${xhr.statusText}`);
            } else {
              let response = xhr.response;
              let span = document.createElement('span');
              span.classList.add('star-wars-players');
              span.style.color = 'orange';
              span.innerText = `${response.name}, `;
              container[3].append(span);
            }
          };
          xhr.onerror = function () {
            alert('People undefined')
          }
        })
      } else if (event.target === titleClass[4]) {
        titleClass[4].style.display = 'none';

        let people = response.results[4].characters;

        people.forEach((item) => {
          let xhr = new XMLHttpRequest();
          xhr.open('GET', item);
          xhr.responseType = 'json';
          xhr.send();
          xhr.onload = function () {
            if (xhr.status !== 200) {
              console.log(`${xhr.status}: ${xhr.statusText}`);
            } else {
              let response = xhr.response;
              let span = document.createElement('span');
              span.classList.add('star-wars-players');
              span.style.color = 'green';
              span.innerText = `${response.name}, `;
              container[4].append(span);
            }
          };
          xhr.onerror = function () {
            alert('People undefined')
          }
        })
      } else if (event.target === titleClass[5]) {
        titleClass[5].style.display = 'none';

        let people = response.results[5].characters;

        people.forEach((item) => {
          let xhr = new XMLHttpRequest();
          xhr.open('GET', item);
          xhr.responseType = 'json';
          xhr.send();
          xhr.onload = function () {
            if (xhr.status !== 200) {
              console.log(`${xhr.status}: ${xhr.statusText}`);
            } else {
              let response = xhr.response;
              let span = document.createElement('span');
              span.classList.add('star-wars-players');
              span.style.color = 'grey';
              span.innerText = `${response.name}, `;
              container[5].append(span);
            }
          };
          xhr.onerror = function () {
            alert('People undefined')
          }
        })
      } else if (event.target === titleClass[6]) {
        titleClass[6].style.display = 'none';

        let people = response.results[6].characters;

        people.forEach((item) => {
          let xhr = new XMLHttpRequest();
          xhr.open('GET', item);
          xhr.responseType = 'json';
          xhr.send();
          xhr.onload = function () {
            if (xhr.status !== 200) {
              console.log(`${xhr.status}: ${xhr.statusText}`);
            } else {
              let response = xhr.response;
              let span = document.createElement('span');
              span.classList.add('star-wars-players');
              span.style.color = 'sky';
              span.innerText = `${response.name}, `;
              container[6].append(span);
            }
          };
          xhr.onerror = function () {
            alert('People undefined')
          }
        })
      } else if (event.target === titleClass[7]) {
        titleClass[7].style.display = 'none';

        let people = response.results[7].characters;

        people.forEach((item) => {
          let xhr = new XMLHttpRequest();
          xhr.open('GET', item);
          xhr.responseType = 'json';
          xhr.send();
          xhr.onload = function () {
            if (xhr.status !== 200) {
              console.log(`${xhr.status}: ${xhr.statusText}`);
            } else {
              let response = xhr.response;
              let span = document.createElement('span');
              span.classList.add('star-wars-players');
              span.style.color = 'red';
              span.innerText = `${response.name}, `;
              container[7].append(span);
            }
          };
          xhr.onerror = function () {
            alert('People undefined')
          }
        })
      }
    });
  }
};



xhr.onerror = function () {
  alert("Запрос не удался");
};
